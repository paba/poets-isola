We outline a use case that we implemented in a trial with a small
business called \emph{Legejunglen}, an indoor playground for children.
 
The user interface to the POETS system is provided by a client application for
the Android operating system. The application is suitable for both phone and
tablet devices. Although, for this trial we focused on the tablet user
experience. The client application communicates with the POETS system running on
a server via the APIs of individual subsystems as described in
Section~\ref{sec:extended-poets}. The client provides a generic user interface
guided by the ontology. There is functionality to visualise ontology elements
as well as allowing user input of ontology elements.  Additionally, a simple
mechanism for compile-time specialised visualisations is provided. The generic
visualisations handle ontology changes without any changes needed on the
client. The central part of the user interface provides an overview of the
state of currently instantiated contract templates as well as allowing users to
interact with running contracts.

In the following, we present the final results of an iterative refinement
process on modelling the \emph{Legejunglen} business. We conclude with some
reflections on using the DSLs for iterative model evolution.

The most important functionality for day-to-day use at
\emph{Legejunglen} is to (1) register bookings for customers and (2)
to get an overview of the scheduled events for a single day. Apart
from that, the system should provide standard accounting functionality.

The main workflow that we needed to implement is the booking system,
that is, the system according to which a customer reserves a time at
the playground. This workflow is encoded in a contract template
\recordname{Appointment}. The data associated with this contract are
defined in the following ontology definition:
\begin{lstlisting}[language=ontology,basicstyle=\small]
Appointment is a Contract.
Appointment is abstract.
Appointment has a DateTime called arrivalDate.
Appointment has Food.
Appointment has a Location called placement.
Appointment has a Participants.
Appointment has an Int called numberOfTableSettings.
Appointment has a String called comments.
Appointment has an Adult entity called contactPerson.
\end{lstlisting}

The full ontology also contains declarations that define the auxiliary
concepts \recordname{Food}, \recordname{Location},
\recordname{Participants} and \recordname{Adult}, which we have elided
here. The fields that are associated with the \recordname{Appointment}
record type have to be provided in order to instantiate the
corresponding $\mathit{Appointment}$ contract template. These fields
are then directly accessible in the definition of the contract
template.

\begin{figure}
\begin{lstlisting}[language=pcsl,xleftmargin=0pt,basicstyle=\small]
name: appointment
type: Appointment
description: "Contract for handling a appointment."

// A reference to the designated entity that represents the company
val me = reports.me ()

clause confirm(expectedArrival : Duration, numberOfTableSettings : Int)
              <me : <Me>, contact : <Adult>> = 
  when ContactConfirms
       due within expectedArrival <-> 1D
       remaining newDeadline
  then
       arrival(newDeadline)<me, contact>
  else arrival(expectedArrival)<me, contact>

clause arrival(expectedArrival : Duration)<me : <Me>, contact : <Adult>> =
  <me> GuestsArrive
       due within expectedArrival <+> 1H
  then payment(me)<contact>

clause payment(me : <Me>)<contact : <Adult>> =
  <contact> Payment(sender s, receiver r)
            where r == me && s == contact
            due within 1D

contract = confirm(subtractDate arrivalDate contractStartDate, 
                   numberOfTableSettings)<me, contactPerson>
\end{lstlisting}
  \caption{Contract template for booking an appointment.}
  \label{fig:arrange}
\end{figure}

Figure~\ref{fig:arrange} details the definition of the contract
template that describes the workflow for booking an appointment at
\emph{Legejunglen}. The full contract is defined at the very bottom by
referring to the $\mathit{confirm}$ clause. Note that we directly
reference the \fieldname{arrivalDate},
\fieldname{numberOfTableSettings} and \fieldname{contactPerson} field
of the \recordname{Appointment} record. The three clauses of the
contract template roughly correspond to three states an active
$\mathit{Appointment}$ contract may be in: first, in the
$\mathit{confirm}$ clause we wait for confirmation from the customer
until one day before the expected arrival. After that we wait for the
arrival of the customer at the expected time (plus a one hour
delay). Finally, we expect the payment within one day.

Next we turn to the reporting functionality of POETS. For daily
planning purposes, \emph{Legejunglen} requires an overview of the
booked appointments of any given day. This functionality is easily
implemented in the reporting language. Firstly, we define the record
type that contains the result of the desired report:
\begin{lstlisting}[language=ontology,basicstyle=\small]
Schedule is a Report.
Schedule has a list of Appointment called appointments.
\end{lstlisting}

Secondly, we define the actual report function that searches the event
log for the creation of $\mathit{Appointment}$ contracts with a given
\fieldname{arrivalDate}. The report definition is given in
Figure~\ref{fig:dailySchedule}.
\begin{figure}
\begin{lstlisting}[language=parrot,xleftmargin=0pt,basicstyle=\small]
name: DailySchedule
description:
	Returns a list of appointments for which the expected
        arrival is the same as the given date.
tags: legejunglen

report : Date -> Schedule
report expectedArrival = 
	Schedule { appointments = [arra |
			putC : PutContract <- events,
			arra : Appointment = putC.contract,
			expectedArrival == arra.arrivalDate.date ] }

\end{lstlisting}
  \caption{Report definition for compiling a daily schedule.}
  \label{fig:dailySchedule}
\end{figure}

A more complex report specification is given in
Figure~\ref{fig:monthlyOverview}. This report compiles an overview of
all appointments made during a month as well as the sum of all
payments that were registered by the system during that time. This
report specification uses an explicit \emph{fold} in order to
accumulate the payment and appointment information that are spread
throughout the event log.
\begin{figure}
\begin{lstlisting}[language=ontology,basicstyle=\small]
MonthlyOverview is a Report.
MonthlyOverview has a Real called total.
MonthlyOverview has a list of AppointmentInfo called appointments.
\end{lstlisting}
\begin{lstlisting}[language=parrot,xleftmargin=0pt,basicstyle=\small]
name: MonthlyOverview
description:
  Get information about payments received for given month.
tags: legejunglen

allContracts : [PutContract]
allContracts = [pc |
  cc : CreateContract <- events,
  pc = first cc [uc | uc : UpdateContract <- events, uc.contractId == cc.contractId]]

allPayments : Date -> [(Payment, PutContract)]
allPayments date =
  [(pay, putC) |
    putC <- allContracts,
    arra : Appointment = putC.contract,
    arra.arrivalDate.month == date.month,
    arra.arrivalDate.year == date.year,
    tr   : TransactionEvent <- transactionEvents,
    tr.contractId == putC.contractId,
    pay : Payment = tr.transaction  ]

initialOverview = MonthlyOverview { total            = 0,
                                                 appointments = [] }


addAppointment : (Payment, Appointment) -> [AppointmentInfo] -> [AppointmentInfo]
addAppointment payArr arrs = insertProj 
  (\pa -> pa.appointment.arrivalDate) 
  (AppointmentInfo {appointment = payArr.2, payment = payArr.1})
  arrs

calc payPut overv = 
  type x = payPut.2.contract of
    Appointment -> overv {
      total              = overv.total + payPut.1.money.amount,
      appointments  = addAppointment (payPut.1, x) overv.appointments }
    _ -> overv

report : Date -> MonthlyOverview
report date = fold calc initialOverview (allPayments date)
\end{lstlisting}
  \caption{Report definition for compiling a monthly payment overview.}
  \label{fig:monthlyOverview}
\end{figure}

Although \emph{Legejunglen} is a relatively simple business, a significant
amount of the work done in the trial involved refining the workflows implicitly
in use and formalising what reports were needed. The ability to specify a
workflow using the contract language and then immediately try it out in the
Android client, helped the modelling process tremendously. A basic contract
template for keeping track of bookings was made quickly, which facilitated the process of iterative evaluation and refinement to precisely capture the way \emph{Legejunglen} worked. 
Changes on the POETS side were quite easy to perform.
Changes are typically isolated. That is, support for new workflows or reports
does not require a change to the data model and only amounts to adding new
contract templates respectively report specifications. This can be performed
while the system is up and running, without any downtime. In addition, the
subtyping discipline employed in POETS' data model is a key feature in
enabling extending the ontology of at run time without compromising the
integrity of its state or the semantics of its reports and contracts.

The effort for implementing changes in the data model and the workflow is quite
modest. Minor changes in the requirements tended to require little changes in
the ontology and contract specifications. Typically, this is also the case for
changes in the report specifications. However, changes in report specifications
turned out to be quite complicated in some instances. Reports have the ability
to produce highly structured information from the flat-structured event log.
Unfortunately, this ability is reflected in the complexity of the corresponding
report specifications. Nonetheless, from the report specifications we have
written, we can extract a small set of high-level patterns that cover most
common use cases. Integrating these high-level patterns into the reporting
language should greatly reduce the effort for writing reports and further
increase readability.

Changes in the underlying modelling on the POETS side were rather easy to
propagate to the Android client software. As mentioned, the client application
provides a generic user interface to the POETS system that allows it to reflect
any changes made in the modelling in the POETS system. However, this generic
interface does not always provide the optimal user experience and therefore
needs manual refinement to reflect changes in the modelling. Additionally,
there have also been specific requirements to the client software, which had to
be implemented. 

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "paper"
%%% End: 
