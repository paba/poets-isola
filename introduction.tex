Enterprise Resource Planning (ERP) systems are comprehensive software
systems used to integrate and manage 
business activities in enterprises. Such
activities include---but are not limited to---financial management
(accounting), production planning, supply chain management and
customer relationship management. ERP systems emerged as a remedy to
heterogeneous systems, in which data and functionality are spread
out---and duplicated---amongst dedicated subsystems. Instead, an ERP
system it built around a central database, which stores all
information in one place.

Traditional ERP systems such as
Microsoft Dynamics
NAV\footnote{\url{http://www.microsoft.com/en-us/dynamics/products/nav-overview.aspx}.},
Microsoft Dynamics
AX\footnote{\url{http://www.microsoft.com/en-us/dynamics/products/ax-overview.aspx}.}, and
SAP\footnote{\url{http://www.sap.com}.}
are three-tier architectures with a client, an application
server, and a centralised relational database system. The central database
stores information in tables, and the application server provides the
business logic, typically coded in a general purpose, imperative
programming language. 
%A shortcoming of this approach is
%that the state of the system is decoupled from business processes, which are not represented explicitly in the system. Rather,
%business processes are encoded implicitly as transition systems, where
%their collective state is maintained by tables in the database, and transitions are
%encoded in the application server, possibly spread out across several
%different modules.

The process-oriented event-driven transaction systems (POETS)
architecture introduced by Henglein et al.~\cite{henglein09jlap} is a
qualitatively different approach to ERP systems. Rather than storing
both transactional data and implicit process state in a database,
POETS employs a pragmatic separation between transactional data, which
is persisted in an \emph{event log}, and \emph{contracts}, which are
explicit representations of business processes, stored in a separate
module. Moreover, rather than using general purpose programming
languages to specify business processes, POETS utilises a declarative
domain-specific language (DSL)~\cite{andersen06sttt}. The use of a DSL
not only enables compositional construction of formalised business processes, it minimises the semantic gap between requirements and a running system, and it facilitates treating processes as data for analysis.  Henglein et al.\ take it as a goal of POETS that ``[...] the formalized
requirements \emph{are} the system''  \cite[page 382]{henglein09jlap}.

The bird's-eye view of the POETS architecture is presented in
Figure~\ref{fig:origArch}.
\begin{figure}[t]
  \centering\small
  \input{origArch}
  \caption{Bird's-eye view of the POETS
    architecture (diagram copied from~\cite{henglein09jlap}).}
  \label{fig:origArch}
\end{figure}
At the heart of the system is the event log, which is an append-only
list of transactions. Transactions represent ``things that take
place'' such as a payment by a customer, a delivery of goods by a
shipping agency, or a movement of items in an inventory. The
append-only restriction serves two purposes. First, it is a legal
requirement in ERP systems that transactions, which are relevant for
auditing, are retained. Second, the report engine utilises
monotonicity of the event log for optimisation, as shown by Nissen and
Larsen~\cite{nissen07ifl}.

%Whereas the event log stores historical data, contracts play the role
%of describing which events are expected in the future. For instance, a
%yearly payment of value-added tax (VAT) to the tax authorities is an
%example of a (recurring) business process. The amount to be paid to
%the tax authorities depends, of course, on the financial transactions
%that have taken place. Therefore, information has to be derived from
%previous transactions in the event log, which is realised as a
%\emph{report}. A report provides structured data derived from the
%transactions in the event log. Like contracts, reports are written in
%a declarative domain-specific language---not only in order to minimise
%the semantic gap between requirements and the running system, but also
%in order to perform incrementalisation, which automatically transforms bulk queries into continuous queries on the event stream \cite{nissen07ifl}.

Besides the radically different software architecture, POETS
distinguishes itself from existing ERP systems by abandoning
the double-entry bookkeeping (DEB) accounting
principle~\cite{weygandt04book} in favour of the Resources, Events,
and Agents (REA) accounting model of McCarthy~\cite{mccarthy82tar}.

%In DEB, each transaction is recorded as two or more
%financial postings in \emph{accounts}, typically a \emph{debit} and a corresponding
%\emph{credit}. When, for instance, a customer pays an amount $x$ to
%a company, then a debit of $x$ is posted in a cash account, and a
%credit of $x$ is posted in a sales account, which reflects the flow of
%cash from the customer to the company. The central invariant of DEB
%is that the credit of \emph{each} transaction equals its debit and thus the total credit across all accounts at any point in time equals its total debit.  
%%DEB is the
%%de facto standard accounting method, and therefore used by
%%current ERP systems.
%
%In REA, transactions are not registered as derivative financial postings in accounts, but rather as the
%actual source events that take place. A payment event in REA is of the form $\mbox{transfer}(a_1,a_2,r,t)$
%meaning that agent $a_1$ transfers resource $r$ to agent $a_2$. For example, when a customer $a$ pays an amount $x$ to a company $c$ at time $t$, it is represented by the single event 
%$\mbox{transfer}(a,c,x,t)$. 
%%Since events are atomic, REA
%%does not have the same redundancy\footnote{In traditional DEB,
%%  redundancy is a feature to check for consistency. However,
%%  in a computer system such redundancy is superfluous.} as DEB, and
%%inconsistency is not a possibility: resources always have an origin and a
%%destination.  
%The POETS architecture not only fits with the REA
%ontology, it was motivated by it. Events are stored as first-class objects
%in the event log, and contracts describe the admissible future sequences of events.\footnote{Structured contracts are not part of the original
%  REA ontology but instead due to Andersen et
%  al.~\cite{andersen06sttt}.} Reports enable computation of derived 
%information for backwards compatibility with DEB, which may be a legal or de facto professional requirement for auditing. For instance, accounts receivable, which summarises (pending) sales payments, can be reconstructed from
%information about initiated sales and payments made by customers so far. Such
%a computation will yield the same \emph{derived} information as in
%DEB, and the principles of DEB consistency are fulfilled by
%construction from the event log.

\subsection{Outline and Contributions}

The motivation for our work is to assess the POETS architecture in
terms of a prototype implementation. During the implementation process
we have added 
features for dynamically managing values and entities to the original architecture. Moreover, in
the process we found that the architecture need not be tied to the
REA ontology---indeed to ERP systems---but can be viewed as a discrete event modelling framework.  Its adequacy for other domains remains future research, however.

Our contributions are as follows:
\begin{itemize}
\item We present a generalised and extended POETS architecture
  (Section~\ref{sec:extended-poets}) that has been fully
  implemented.
\item We present domain-specific languages for data modelling
  (Section~\ref{sec:data-model}), report specification
  (Section~\ref{sec:report-engine}), and contract specification
  (Section~\ref{sec:contract-engine}).
\item We illustrate small use case that we have implemented in our
  system as part of a trial for a small business
  (Section~\ref{sec:use-case}).
\end{itemize}

The POETS server system has been implemented in
Haskell. Its client code has been developed in Java, primarily for Android. The choice of Haskell, specifically the Glasgow Haskell Compiler (GHC), is due to: the conciseness, affinity and support of functional programming for enterprise software \cite{murthy2007advanced} and declarative DSL implementation;  
its expressive type system, which supports statically typed solutions to the Expression Problem \cite{bahv2012,bahv2011}; and competitive run-time performance due to advanced compiler optimisations in GHC.  The use of Java on the client side (not further discussed in this paper) arises from POETS, conceived to be cloud-based and mobile from the outset, targeting low-cost mobile devices and a practical desire to reuse code as much as possible across smartphones, tablets, portables and desktops.

The source code of this implementation is available from the
repository at \url{https://bitbucket.org/jespera/poets/}. In addition,
the repository also includes the full source code for the use case
presented in Section~\ref{sec:use-case}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "paper"
%%% End: 