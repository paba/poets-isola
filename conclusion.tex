We have presented an extended and generalised version of the POETS
architecture~\cite{henglein09jlap}, which we have fully implemented. It is based on declarative domain-specific languages for specifying the data model, reports, and contracts of a POETS instance, which offer
enterprise domain concepts and encapsulate important invariants that facilitate safe run-time changes to data types, reports and contracts; full recoverability and auditability of any previous system state; and strict separation of logged raw data and efficiently computed user-specified derived data.  In particular, in contrast to its predecessor, 
any historical system state is reestablishable for auditing since also master data, contract and report changes are logged, not only transactional data. 

The use case presented illustrates the conciseness of POETS DSLs and support for rapid exploratory process and report design since the ``specification is the implementation'' approach made it easy to make an initial model of the business as well as evolve it to new requirements.
While no significant conclusions for usability and fitness for use in complex commercial settings can be drawn without a suitable experimental design, we believe the preliminary results justify hypothesising that domain specialists should be able to read, understand and specify data models (types) and, with suitable training in \emph{formalisation}, eventually contract and report specifications without having to worry about programming or system specifics.

\subsection{Related work}

This paper focuses on the radical \emph{use} of declarative domain-specific languages in POETS motivated by the Resources, Event, Agents accounting model \cite{mccarthy82tar,henglein09jlap}.  
The syntactic and semantic aspects of its domain modelling language \cite{thomsen2010}, its contract language \cite{Hvitved201272} (evolved from \cite{andersen06sttt}) and 
functional reporting\footnote{Automatic incrementalisation is not implemented in the present version.} \cite{nissen07ifl,nissen2008a} 
are described elsewhere.  

ERP systems relate broadly to and combine aspects of discrete event simulation, workflow modelling, choreography and orchestration,
run-time monitoring, process specification languages (such as LTL), process models (such as Petri nets), and report languages (such as the query sublanguage of SQL and reactive functional programming frameworks), which makes a correspondingly extensive review of related work from a general ERP systems point of view a difficult and expansive task.  

More narrowly, POETS can be considered an example of \emph{language-oriented programming} \cite{ward1994language} applied to the business modelling domain. 
Its contract language specifies detailed real-time and value
constraints (e.g.\ having to pay the cumulatively correct amount by
some deadline, not just some amount at some time) on contract
partners, neither supporting nor fixing a particular business process.
See \cite[Chapter 1]{hvitved2011} and \cite{hvitved2010survey} for a survey of \emph{contract} models and languages.

A hallmark of POETS is its enforcement of static invariants that guarantee auditability and type correctness even in the presence of run-time updates to data types, processes and reports.  Recently the jABC approach \cite{steffen2007model,margaria2009business} has added support for types, data-flow modelling and processes as first-class citizens.  The resulting DyWA (Dynamic Web Application) approach \cite{6587453,nsm2013,nsfm2014} offers support for step-by-step run-time enhancement with data types and corresponding business processes until an application is ready for execution and for its subsequent evolution. 

Automatic incrementalisation of report functions in POETS can be thought of as translating bulk-oriented queries that conceptually inspect the complete event log every time they are run to continuous queries on streams \cite{terry1992continuous}, based on formal differentiation techniques \cite{pako,liu2000}.  

%
%Value points:
%
%Contracts fully specified, both resources and real time requirements.
%
%Reports in purely functional language, facilitates automatic incrementalization.  Auxiliary data structures derived.
%Raw and derived data strictly separated; when reports are removed, auxliary data can be safely 'garbage-collected'.
%
%Type-safe run-time additions and changes of data types.  Support rapid business process development.
%
%Any state reestablishable for auditing since also master data, contract and report changes are logged. 
%

\subsection{Future Work}
\label{sec:future-work}

\paragraph{Expressivity}
A possible extension of the data model is to introduce finite maps,
which will enable a modelling of resources that is closer in structure
to that of Henglein et al.~\cite{henglein09jlap}.  Another possible
extension is to allow types as values in the report language. There
are instances where we currently use a string representation of record
types rather than the record types themselves. This representation is,
of course, suboptimal: we would like such runtime represenations of
types machine checked and take subtyping into account.

\paragraph{Rules}
A rule engine is a part of our extended architecture
(Figure~\ref{fig:arch}), however it remains to be implemented. The
purpose of the rule engine is to provide rules---written in a separate
domain-specific language---that can constrain the values that are
accepted by the system. For instance, a rule might specify that the
\fieldname{items} list of a \recordname{Delivery} transaction always
be non-empty.

More interestingly, the rule engine will enable values
to be \emph{inferred} according to the rules in the engine. For instance, a
set of rules for calculating VAT will enable the field
\fieldname{vatPercentage} of an \recordname{OrderLine} to be inferred
automatically in the context of a \recordname{Sale} record. That is,
based on the information of a sale and the items that are being sold,
the VAT percentage can be calculated automatically for each item
type.

The interface to the rule engine will be very simple: a record value
with zero or more \emph{holes} is sent to the engine, and the engine
will return either
\begin{inparaenum}[(i)]
\item an indication that the record cannot possibly fulfil the rules in the
  engine, or
\item a (partial) substitution that assigns inferred values to (some
  of) the holes of the value as dictated by the rules.
\end{inparaenum}
Hence when we, for example, instantiate the sale of a bicycle. then we
first let the rule engine infer the VAT percentage before passing the
contract meta data to the contract engine.

\paragraph{Forecasts}
A feature of the contract engine, or more specifically of the reduction
semantics of contract instances, is the possibility to retrieve the
state of a running contract at any given point in time. The state is
essentially the AST of a contract clause, and it describes what is
currently expected in the contract, as well as what is expected in the
future.

Analysing the AST of a contract enables the possibility to do
\emph{forecasts}, for instance to calculate the expected outcome of a
contract or the items needed for delivery within the next
week. Forecasts are, in some sense, dual to reports. Reports derive
data from transactions, that is, facts about what has previously
happened. Forecasts, on the other hand, look into the future, in terms
of calculations over running contracts. We have currently implemented
a single forecast, namely a forecast that lists the set of immediately
expected transactions for a given contract. A more ambitious approach
is to devise (yet another) language for writing forecasts, that is,
functions that operate on contract ASTs.

\paragraph{Practicality}
In order to make POETS useful in practice, many features are still 
missing. However, we see no inherent difficulties in adding them to
POETS compared to traditional ERP architectures. To mention a few:
\begin{inparaenum}[(i)]
\item security, that is, authorisation, users, roles, etc.;
\item module systems for the report language and contract language,
  that is, better support for code reuse; and
\item check-pointing of a running system, that is, a dump of the memory
  of a running system, so the event log does not have to be replayed
  from scratch when the system is restarted.
\end{inparaenum}

\paragraph{Acknowledgements} Morten Ib Nielsen and Mikkel J\o{}nsson
Thomsen both contributed to our implementation and design of POETS,
for which we are thankful. We thank the participants of the
DIKU course ``POETS Summer of Code'' and Lejejunglen for testing, use and valuable input to POETS.  This work has been made possible by a grant by the Danish National Advanced Technology Foundation (H\o jteknologifonden) for Project 3gERP.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "paper"
%%% End: 
